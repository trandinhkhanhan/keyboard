const express = require('express');
const jwt = require('jsonwebtoken');
const axios = require('axios');
const rateLimit = require('express-rate-limit');
const Redis = require('ioredis');
const CircuitBreaker = require('circuit-breaker-js');

const { JWT_SECRET, REDIS_URL, RATE_LIMIT_WINDOW_MS, RATE_LIMIT_MAX_REQUESTS } = require('./config');

const app = express();
const redis = new Redis(REDIS_URL);

// Middleware để kiểm tra JWT
const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, JWT_SECRET, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }

            req.user = user;
            next();
        });
    } else {
        res.sendStatus(401);
    }
};

// Middleware giới hạn tốc độ truy cập
const limiter = rateLimit({
    windowMs: RATE_LIMIT_WINDOW_MS,
    max: RATE_LIMIT_MAX_REQUESTS,
    message: 'Too many requests, please try again later.',
});

// Circuit Breaker cho service API call
const circuitBreaker = new CircuitBreaker(axios.get, {
    timeoutDuration: 10000, // 10 giây
    volumeThreshold: 5,
    errorThreshold: 50,
    enabled: true,
});

app.use(express.json());

// API endpoint bảo vệ bởi JWT và giới hạn tốc độ truy cập
app.get('/api/protected', authenticateJWT, limiter, async (req, res) => {
    try {
        const response = await axios.get('http://api-service.com/data');
        res.json(response.data);
    } catch (error) {
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

// API endpoint sử dụng Circuit Breaker
app.get('/api/service', async (req, res) => {
    try {
        const response = await circuitBreaker.fire('http://api-service.com/data');
        res.json(response.data);
    } catch (error) {
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

// Endpoint cho các service mới
app.post('/api/publish', async (req, res) => {
    // Logic xử lý phát hành phím
});

app.get('/api/keyboard', async (req, res) => {
    // Logic xử lý thông tin về các loại phím
});

app.get('/api/user/:userId', async (req, res) => {
    // Logic xử lý thông tin người dùng
});

app.get('/api/maker/:makerId', async (req, res) => {
    // Logic xử lý thông tin chủ phím
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
