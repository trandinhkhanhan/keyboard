module.exports = {
    JWT_SECRET: 'your_jwt_secret',
    REDIS_URL: 'redis://localhost:6379',
    RATE_LIMIT_WINDOW_MS: 60000, // 1 phút
    RATE_LIMIT_MAX_REQUESTS: 100, // 100 requests trong 1 phút
};
