const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

const keys = [];

app.post('/api/keys', (req, res) => {
    const { key } = req.body;
    keys.push(key);
    res.json({ message: 'Key stored successfully' });
});

app.get('/api/keys', (req, res) => {
    res.json({ keys });
});

const PORT = process.env.PORT || 4002;
app.listen(PORT, () => {
    console.log(`Storage Service is running on port ${PORT}`);
});
