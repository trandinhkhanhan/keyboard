const express = require('express');

const app = express();

const makers = [
    { id: 1, name: 'Maker1' },
    { id: 2, name: 'Maker2' },
];

app.get('/api/makers', (req, res) => {
    res.json({ makers });
});

app.get('/api/makers/:makerId', (req, res) => {
    const { makerId } = req.params;
    const maker = makers.find(m => m.id === parseInt(makerId));
    if (!maker) {
        return res.status(404).json({ message: 'Maker not found' });
    }
    res.json({ maker });
});

const PORT = process.env.PORT || 4004;
app.listen(PORT, () => {
    console.log(`Maker Service is running on port ${PORT}`);
});
