const express = require('express');

const app = express();

const keyboards = [
    { id: 1, brand: 'Logitech', model: 'G Pro X' },
    { id: 2, brand: 'Razer', model: 'BlackWidow Elite' },
];

app.get('/api/keyboards', (req, res) => {
    res.json({ keyboards });
});

app.get('/api/keyboards/:keyboardId', (req, res) => {
    const { keyboardId } = req.params;
    const keyboard = keyboards.find(k => k.id === parseInt(keyboardId));
    if (!keyboard) {
        return res.status(404).json({ message: 'Keyboard not found' });
    }
    res.json({ keyboard });
});

const PORT = process.env.PORT || 4005;
app.listen(PORT, () => {
    console.log(`Keyboard Service is running on port ${PORT}`);
});
