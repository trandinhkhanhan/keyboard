const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

app.post('/api/publish', (req, res) => {
    // Logic xử lý phát hành phím từ request body
    res.json({ message: 'Key published successfully' });
});

const PORT = process.env.PORT || 4001;
app.listen(PORT, () => {
    console.log(`Publish Service is running on port ${PORT}`);
});
